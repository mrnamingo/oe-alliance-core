KV = "5.0.0"
SRCDATE = "20190402"
KOFILES = "brcmstb-${MACHINE} ci si2183 avl6862 avl6261"

require edision-dvb-modules.inc
SRC_URI[md5sum] = "d91e9cbf0071944a383cc8c1cc674d48"
SRC_URI[sha256sum] = "9efe35a3b4e619602edad64f5d587207866b98698560bc59a813f960f603b934"
